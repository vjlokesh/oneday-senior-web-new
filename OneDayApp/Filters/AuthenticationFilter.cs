﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using NLog;
using OneDay.BusinessService.ServiceAbstract;
using OneDay.DataService.Abstract;
using OneDayApp.Scheme;

namespace OneDayApp.Filters
{
    public class AuthenticationFilter : ActionFilterAttribute
    {
        private readonly ILogger<AuthenticationFilter> _logger;
        private IAccountService _accountService;
        private const string Token = "Token";

        public AuthenticationFilter(IAccountService accountService)
        {
            //_logger = LogManager.GetCurrentClassLogger();
            _accountService = accountService;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionDescriptor.FilterDescriptors.Any(t =>
                t.Filter.GetType() == typeof(Microsoft.AspNetCore.Mvc.Authorization.AllowAnonymousFilter)))
                return;

            if (filterContext.HttpContext.Request.Headers[Token].Count == 1)
            {
                var decryptedValue = filterContext.HttpContext.Request.Headers[Token].First().Decrypt();
                var serializeModel = JsonConvert.DeserializeObject<CustomPrincipalModel>(decryptedValue);

                StringValues deviceIds;
                var valid = false;
                if (filterContext.HttpContext.Request.Headers.TryGetValue("X-device-id", out deviceIds))
                {
                    valid = TokenValidate(serializeModel.Token, 300, deviceIds.First());
                }
                else
                {
                    valid = TokenValidate(serializeModel.Token, 300);
                }

                if (valid)
                {
                    var newUser = new CustomPrincipal(Guid.NewGuid().ToString())
                    {
                        UserId = serializeModel.UserId,
                        FirstName = serializeModel.FirstName,
                        LastName = serializeModel.LastName,
                        RoleNameList = serializeModel.RoleNameList,
                        Token = serializeModel.Token,
                        Email = serializeModel.Email,
                        RememberMe = serializeModel.RememberMe,
                        ExternalId = serializeModel.ExternalId,
                        CommunityIdList = serializeModel.CommunityIdList,
                        RoleIdList = serializeModel.RoleIdList,

                        CompanyId = serializeModel.CompanyId

                    };

                    ClaimsPrincipal principal = new ClaimsPrincipal(newUser);
                    filterContext.HttpContext.User = principal;
                }
                else
                {

                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    filterContext.Result = new ContentResult()
                    {
                        StatusCode = 401,
                        Content = "Unauthorized Access."
                    };
                }
            }
            else
            {
                filterContext.Result = new ContentResult()
                {
                    StatusCode = 401,
                    Content = "Unauthorized Access."
                };
            }

            base.OnActionExecuting(filterContext);
        }

        private bool TokenValidate(Guid authToken, int authTokenExpiry, string iAppId = null)
        {

            return _accountService.ValidateToken(authToken, authTokenExpiry, iAppId);
           
        }
    }
}
