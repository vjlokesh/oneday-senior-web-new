﻿using System;

namespace OneDayApp.Core.Domain
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        public string Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ExternalId { get; set; }

        public DateTime? BirthDate { get; set; }

        public bool? Active { get; set; }

        public string PhoneNumber { get; set; }
    }
}
