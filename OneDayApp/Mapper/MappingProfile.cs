﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using OneDay.BusinessService.Domain;
using OneDay.DataService.Domain;
using OneDay.DataService.Models;

namespace OneDayApp.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<List<CompanyStat>, List<CompanyStatViewModel>>();
        }

    }
}
