﻿using System;
using System.Collections.Generic;
using System.Security.Principal;


namespace OneDayApp.Scheme
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomPrincipalModel
    {

        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid Token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ExternalId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool RememberMe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> RoleNameList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<short> RoleIdList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> CommunityIdList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? CompanyId { get; set; }


    }
    /// <summary>
    /// 
    /// </summary>
    public class CustomPrincipal : IPrincipal
    {
        /// <summary>
        /// 
        /// </summary>
        public IIdentity Identity { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            return RoleNameList.IsInRole(role);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool IsInRole(short roleId)
        {
            return RoleIdList.IsInRole(roleId);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public CustomPrincipal(string email)
        {
            Identity = new GenericIdentity(email);
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid Token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ExternalId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool RememberMe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> RoleNameList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<short> RoleIdList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> CommunityIdList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? CompanyId { get; set; }

    }
}
