﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace OneDayApp.Scheme
{
    public static class EncryptionExtension
    {
        public static string Encrypt(this int? val)
        {
            if (val == null)
                return string.Empty;
            return val.ToString().Encrypt();
        }

        public static string Encrypt(this int val)
        {

            return val.ToString().Encrypt();
        }

        public static string Encrypt(this string val)
        {
            var rgbIv = Encoding.ASCII.GetBytes("jwgoghjkdxlppfng");
            var key = Encoding.ASCII.GetBytes("rvpunximtiuskqjesdbybfngodtnygwn");

            var clearTextBytes = Encoding.UTF8.GetBytes(val);
            var rijn = SymmetricAlgorithm.Create();
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, rijn.CreateEncryptor(key, rgbIv), CryptoStreamMode.Write);

            cs.Write(clearTextBytes, 0, clearTextBytes.Length);
            cs.Close();

            return Convert.ToBase64String(ms.ToArray());
        }

        public static string Decrypt(this string val)
        {
            if (val == null)
                return "";
            var rgbIv = Encoding.ASCII.GetBytes("jwgoghjkdxlppfng");
            var key = Encoding.ASCII.GetBytes("rvpunximtiuskqjesdbybfngodtnygwn");
            val = val.Replace(" ", "+");
            var encryptedTextBytes = Convert.FromBase64String(val);
            var ms = new MemoryStream();
            var rijn = Aes.Create();

            var cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIv),
            CryptoStreamMode.Write);

            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }
}
