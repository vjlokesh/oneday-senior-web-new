﻿using System.Collections.Generic;

namespace OneDayApp.Scheme
{
    public static class CustomPrincipalExtension
    {
        public static bool IsInRole(this  List<string> roleNameList, string role)
        {
            return roleNameList.Contains(role);
        }
        public static bool IsInRole(this List<short> roleIdList, short roleId)
        {
            return roleIdList.Contains(roleId);
        }
    }
}
