﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OneDay.BusinessService.Domain;
using OneDay.BusinessService.ServiceAbstract;


namespace OneDayApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Company")]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        protected readonly ILogger<CompanyController> _logger;
        public CompanyController(ICompanyService companyService, ILogger<CompanyController> logger)
        {
            _companyService = companyService;
            _logger = logger;
        }
        // GET: api/Company
        [HttpGet]
        public List<CompanyStatViewModel> Get()
        {
            _logger.LogInformation("Getting company information");
;
            return _companyService.GetAllCompanies();
        }

        // GET: api/Company/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Company
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Company/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
