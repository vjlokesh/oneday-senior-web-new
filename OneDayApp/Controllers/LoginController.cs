﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OneDay.BusinessService.ServiceAbstract;
using OneDay.DataService.Abstract;
using OneDayApp.ViewModels;


namespace OneDayApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private IAccountService _accountService;

        public LoginController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        public void Index()
        {
            
        }

        // POST: api/Company
        [HttpPost]
        public string Post([FromBody]LoginViewModel value)
        {
            var user= _accountService.FindByCredentials(value.Email, value.Password);
            if (user == null)
            {
                return "User not availbale";
            }
            else
            {
                return "Login Success";
            }
        }
    }
}