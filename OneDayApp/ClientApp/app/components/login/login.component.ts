import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import {LoginViewModel} from '../../models/LoginViewModel';


@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    public loginViewModel : LoginViewModel;
    public _http: Http;
    public _baseUrl: string;
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.loginViewModel = new LoginViewModel();
        this._http = http;
        this._baseUrl = baseUrl;
    }

    public LoginUser(model: LoginViewModel) {
        this._http.post(this._baseUrl + 'api/Login', this.loginViewModel).subscribe(result => {
            this.loginViewModel.Result = result.json() as string;
        }, error => { console.error(error); this.loginViewModel.Result="LoginFailed" });
        //this.loginViewModel.Result = model.Email;
    }

}

