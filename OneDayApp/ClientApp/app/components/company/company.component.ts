import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'company',
    templateUrl: './company.component.html'
})
export class CompanyComponent {
    public companyStats: CompanyStat[];

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/Company').subscribe(result => {
            this.companyStats = result.json() as CompanyStat[];
        }, error => console.error(error));
    }
}

interface CompanyStat {
    url: string;
    companyId: number;
    name: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    zip: string;
    userVideos: number;
    newVideos: number;
    videoFeatured: number;
    logins:number;
}
