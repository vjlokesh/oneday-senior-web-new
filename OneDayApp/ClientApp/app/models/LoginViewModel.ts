﻿export class LoginViewModel {
    Email: string;
    Password: string;
    Result: string;

    constructor() {
        this.Email = '';
        this.Password = '';
        this.Result = '';
    }
}