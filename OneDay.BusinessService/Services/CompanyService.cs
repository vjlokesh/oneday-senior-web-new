﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using OneDay.BusinessService.Domain;
using OneDay.BusinessService.ServiceAbstract;
using OneDay.DataService.Abstract;
using OneDay.DataService.Domain;
using OneDay.DataService.Models;

namespace OneDay.BusinessService.Services
{
    public class CompanyService : ICompanyService
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }
        public List<CompanyStatViewModel> GetAllCompanies()
        {
            using (_companyRepository)
            {
                var companyStats= _companyRepository.GetAllCompanies();
                var companyViewModel = Mapper.Map<List<CompanyStat>,List<CompanyStatViewModel>>(companyStats);
                return companyViewModel;
            }
        }
    }
}
