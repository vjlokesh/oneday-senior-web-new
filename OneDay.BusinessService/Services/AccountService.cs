﻿using System;
using System.Collections.Generic;
using System.Text;
using OneDay.DataService.Abstract;
using AutoMapper;
using OneDay.BusinessService.Domain;
using OneDay.BusinessService.ServiceAbstract;
using OneDay.DataService.Models;

namespace OneDay.BusinessService.Services
{
    public class AccountService : IAccountService
    {
        private IAccountRepository _accountRepository;
        public AccountService(IAccountRepository acntRepository)
        {
            _accountRepository = acntRepository;
        }

        public UserViewModel FindByCredentials(string email, string password)
        {
            using (_accountRepository)
            {
               var user= _accountRepository.FindByCredentials(email, password);
                var userModel=Mapper.Map<User,UserViewModel>(user);
                return userModel;
            }
        }

        public bool ValidateToken(Guid authToken, int authTokenExpiry, string iAppId = null)
        {
            using (_accountRepository)
            {
                return _accountRepository.ValidateToken(authToken, authTokenExpiry, iAppId);
            }
        }
    }
}
