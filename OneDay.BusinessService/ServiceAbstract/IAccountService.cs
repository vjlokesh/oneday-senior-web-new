﻿using System;
using OneDay.BusinessService.Domain;

namespace OneDay.BusinessService.ServiceAbstract
{
    public interface IAccountService
    {
        bool ValidateToken(Guid authToken, int authTokenExpiry, string iAppId = null);
        UserViewModel FindByCredentials(string email, string password);
    }
}
