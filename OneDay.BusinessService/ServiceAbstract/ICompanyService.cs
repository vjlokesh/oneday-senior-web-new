﻿using System.Collections.Generic;
using OneDay.BusinessService.Domain;

namespace OneDay.BusinessService.ServiceAbstract
{
    public interface ICompanyService
    {
        List<CompanyStatViewModel> GetAllCompanies();
    }
}
