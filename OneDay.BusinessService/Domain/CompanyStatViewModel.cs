﻿using System;

namespace OneDay.BusinessService.Domain
{
    public class CompanyStatViewModel
    {
        public string Url { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public long? Phone { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public long? ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public int? VideoInvites { get; set; }
        public int? UserVideos { get; set; }
        public int? VideoLikes { get; set; }
        public int? VideoShares { get; set; }
        public int? NewVideos { get; set; }
        public int? VideoViews { get; set; }
        public int VideoFeatured { get; set; }
        public int? Logins { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
