﻿using System;
using System.Collections.Generic;
using OneDay.DataService.Domain;

namespace OneDay.DataService.Abstract
{
    public interface ICompanyRepository: IDisposable
    {
        List<CompanyStat> GetAllCompanies();
    }
}