﻿using OneDay.Utils;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace OneDay.DataService.Abstract
{
    //TODO: Refine for better connection management and DI
    public class BaseRepository 
    {
        private readonly string connString;
        protected IDbConnection DataConnection => FillConnection();

        //public BaseRepository(IDbConnection dataConnection)
        //{
        //    FillConnection(dataConnection);
        //}

        public BaseRepository(string connectionString)
        {
            connString = connectionString;
        }

        private IDbConnection FillConnection()
        {
            IDbConnection dataConnection =new SqlConnection(connString);
            //if (string.IsNullOrEmpty(dataConnection.ConnectionString))
            //{
            //    _dataConnection = new SqlConnection(ConfigurationHelper.GetConnectionString());
            //}
            //else
            //{
            //    _dataConnection = dataConnection;
            //}

            return dataConnection;
        }

    }
}
