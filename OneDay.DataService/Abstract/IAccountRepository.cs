﻿using System;
using OneDay.DataService.Models;

namespace OneDay.DataService.Abstract
{
    public interface IAccountRepository:IDisposable
    {
        bool ValidateToken(Guid authToken, int authTokenExpiry, string iAppId = null);
        User FindByCredentials(string email, string password);
    }
}