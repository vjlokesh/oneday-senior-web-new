namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("UserCompany")]
    public partial class UserCompany
    {
        [Key]
        public int UserCompanyId { get; set; }

        public int UserId { get; set; }

        public int CompanyId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool Active { get; set; }

        public virtual Company Company { get; set; }

        public virtual User User { get; set; }
    }
}
