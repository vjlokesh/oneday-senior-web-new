namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsCategoryStory")]
    public partial class CmsCategoryStory
    {
        [Key]
        public int CmsCategoryStoryId { get; set; }

        public int CmsCategoryId { get; set; }

        public int CmsStoryId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public short SortOrder { get; set; }

        public virtual CmsCategory CmsCategory { get; set; }

        public virtual CmsStory CmsStory { get; set; }
    }
}
