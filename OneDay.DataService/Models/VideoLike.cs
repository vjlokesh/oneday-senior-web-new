namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("VideoLike")]
    public partial class VideoLike
    {
        [Key]
        public int VideoLikeId { get; set; }

        public int VideoId { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool Active { get; set; }

        public virtual User User { get; set; }

        public virtual Video Video { get; set; }
    }
}
