namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CompanyAttribute")]
    public partial class CompanyAttribute
    {
        [Key]
        public int CompanyAttributeId { get; set; }

        public int CompanyId { get; set; }

        [StringLength(50)]
        public string PrimaryColor { get; set; }

        [StringLength(50)]
        public string SecondaryColor { get; set; }

        [StringLength(50)]
        public string TertiaryColor { get; set; }

        [StringLength(50)]
        public string ThemeTitleColor { get; set; }

        [StringLength(50)]
        public string ThemeBackgroundColor { get; set; }

        public int? ThemeCmsAudioId { get; set; }

        public int? LogoMenuCmsImageId { get; set; }

        public CmsImage LogoMenuCmsImage { get; set; }

        public int? VideoPlayerLogoCmsImageId { get; set; }

        public int? StoryBackgroundCmsImageId { get; set; }

        [StringLength(50)]
        public string StoryBackgroundColor { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int? CreatedByUserId { get; set; }

        [StringLength(500)]
        public string AdImageUrl { get; set; }

        [StringLength(25)]
        public string ThemeTitleFont { get; set; }

        public virtual Company Company { get; set; }
    }
}
