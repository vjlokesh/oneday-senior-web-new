namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsStory")]
    public partial class CmsStory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CmsStory()
        {
            CmsCategoryStories = new HashSet<CmsCategoryStory>();
            CmsStoryMoments = new HashSet<CmsStoryMoment>();
            StoryCustomizations = new HashSet<StoryCustomization>();
        }

        [Key]
        public int CmsStoryId { get; set; }

        public int CompanyId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        public byte StatusId { get; set; }

        public int? ThemeId { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int? CreatedByUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsCategoryStory> CmsCategoryStories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsStoryMoment> CmsStoryMoments { get; set; }

        public virtual Company Company { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoryCustomization> StoryCustomizations { get; set; }
    }
}
