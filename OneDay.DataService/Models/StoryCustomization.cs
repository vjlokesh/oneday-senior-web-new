namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class StoryCustomization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StoryCustomization()
        {
            StoryCustomizationMoments = new HashSet<MomentCustomization>();
        }

        [Key]
        public int StoryCustomizationId { get; set; }

        public int CategoryCustomizationId { get; set; }

        public int StoryId { get; set; }

        public int? SortOrder { get; set; }

        [StringLength(250)]
        public string Title { get; set; }

        public virtual CategoryCustomization CategoryCustomization { get; set; }

        public virtual CmsStory CmsStory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MomentCustomization> StoryCustomizationMoments { get; set; }

        public virtual ThemeCustomization ThemeCustomization { get; set; }
    }
}
