namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsAudio")]
    public partial class CmsAudio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CmsAudio()
        {
            CmsThemes = new HashSet<CmsTheme>();
            ThemeCustomizations = new HashSet<ThemeCustomization>();
        }

        [Key]
        public int CmsAudioId { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(1000)]
        public string Url { get; set; }

        [Required]
        [StringLength(255)]
        public string ContentType { get; set; }

        public int FileSize { get; set; }

        public byte StatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int CreatedByUserId { get; set; }

        public int CreatedUserId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsTheme> CmsThemes { get; set; }

        public virtual Company Company { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations { get; set; }
    }
}
