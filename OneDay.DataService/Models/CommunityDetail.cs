namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class CommunityDetail
    {
        [Key]
        public int CommunityDetailsId { get; set; }

        public int? CompanyId { get; set; }

        public int? CommunityId { get; set; }

        public byte StatusId { get; set; }

        [StringLength(300)]
        public string WebLogoUrl { get; set; }

        [StringLength(300)]
        public string WebLogoBackUpUrl { get; set; }

        [StringLength(300)]
        public string MobileLogoUrl { get; set; }

        [StringLength(300)]
        public string BackGroundImageUrl { get; set; }

        [StringLength(300)]
        public string HeaderText { get; set; }

        [StringLength(300)]
        public string SubHeaderText { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedByUserId { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        [StringLength(300)]
        public string UserBackgroundUrl { get; set; }

        public virtual Community Community { get; set; }

        public virtual Company Company { get; set; }

        public virtual Status Status { get; set; }
    }
}
