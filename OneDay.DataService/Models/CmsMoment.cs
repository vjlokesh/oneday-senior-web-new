namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsMoment")]
    public partial class CmsMoment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CmsMoment()
        {
            CmsStoryMoments = new HashSet<CmsStoryMoment>();
            StoryCustomizationMoments = new HashSet<MomentCustomization>();
        }

        [Key]
        public int CmsMomentId { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public byte StatusId { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedByUserId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsStoryMoment> CmsStoryMoments { get; set; }

        public virtual Status Status { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MomentCustomization> StoryCustomizationMoments { get; set; }
    }
}
