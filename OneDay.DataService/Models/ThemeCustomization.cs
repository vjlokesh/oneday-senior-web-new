namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class ThemeCustomization
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StoryCustomization_StoryCustomizationId { get; set; }

        public int CmsThemeId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(255)]
        public string BackgroundColor { get; set; }

        [StringLength(255)]
        public string TitleColor { get; set; }

        [StringLength(255)]
        public string TitleFont { get; set; }

        public int? CmsAudioId { get; set; }

        public int? BackgroundImageId { get; set; }

        public int? IntroImageId { get; set; }

        public int? TitleBackgroundImageId { get; set; }

        public int? OutroImageId { get; set; }

        public virtual CmsAudio CmsAudio { get; set; }

        public virtual CmsImage CmsImage { get; set; }

        public virtual CmsImage CmsImage1 { get; set; }

        public virtual CmsImage CmsImage2 { get; set; }

        public virtual CmsImage CmsImage3 { get; set; }

        public virtual CmsTheme CmsTheme { get; set; }

        public virtual StoryCustomization StoryCustomization { get; set; }
    }
}
