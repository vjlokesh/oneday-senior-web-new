namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Video")]
    public partial class Video
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Video()
        {
            CommunityVideos = new HashSet<CommunityVideo>();
            Recipients = new HashSet<Recipient>();
            UserVideos = new HashSet<UserVideo>();
            VideoLikes = new HashSet<VideoLike>();
            VideoShares = new HashSet<VideoShare>();
            VideoViews = new HashSet<VideoView>();
            InviatationDetails = new HashSet<InviatationDetails>();
        }

        [Key]
        public int VideoId { get; set; }

        [StringLength(50)]
        public string Id { get; set; }

        public Guid? UId { get; set; }

        [Required]
        [StringLength(500)]
        public string ThumbNailPath { get; set; }

        [Required]
        [StringLength(500)]
        public string VideoPath { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public bool IsFeatured { get; set; }

        public int VideoDuration { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        [StringLength(50)]
        public string ShortUrl { get; set; }

        [ForeignKey("AuthorId")]
        public User Author { get; set; }

        public int? AuthorId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommunityVideo> CommunityVideos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recipient> Recipients { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserVideo> UserVideos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoLike> VideoLikes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoShare> VideoShares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoView> VideoViews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InviatationDetails> InviatationDetails { get; set; }
    }
}
