﻿namespace OneDay.DataService.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("InvitationConfirmations")]
    public class InvitationConfirmation
    {
        [Key]
        public int Id { get; set; }

        public InviatationDetails InviatationDetails { get; set; }

        public int InviatationDetailsId { get; set; }

        public string ClientIpAddress { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
