namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class MomentCustomization
    {
        public int MomentCustomizationId { get; set; }

        public int StoryCustomizationId { get; set; }

        public int MomentId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        public int? SortOrder { get; set; }

        public virtual CmsMoment CmsMoment { get; set; }

        public virtual StoryCustomization StoryCustomization { get; set; }
    }
}
