namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsImage")]
    public partial class CmsImage
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CmsImage()
        {
            CmsThemes = new HashSet<CmsTheme>();
            CmsThemes1 = new HashSet<CmsTheme>();
            CmsThemes2 = new HashSet<CmsTheme>();
            CmsThemes3 = new HashSet<CmsTheme>();
            ThemeCustomizations = new HashSet<ThemeCustomization>();
            ThemeCustomizations1 = new HashSet<ThemeCustomization>();
            ThemeCustomizations2 = new HashSet<ThemeCustomization>();
            ThemeCustomizations3 = new HashSet<ThemeCustomization>();
            CompanyAttributes = new HashSet<CompanyAttribute>();
        }

        [Key]
        public int CmsImageId { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(1000)]
        public string Url { get; set; }

        [Required]
        [StringLength(255)]
        public string ContentType { get; set; }

        public int FileSize { get; set; }

        public byte ImageTypeId { get; set; }

        public byte StatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual CmsImageType CmsImageType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsTheme> CmsThemes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsTheme> CmsThemes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsTheme> CmsThemes2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CmsTheme> CmsThemes3 { get; set; }

        public virtual Company Company { get; set; }

        public virtual ICollection<CompanyAttribute> CompanyAttributes { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations3 { get; set; }
    }
}
