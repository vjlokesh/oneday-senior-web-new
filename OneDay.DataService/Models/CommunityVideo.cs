namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class CommunityVideo
    {
        [Key]
        public int CommunityVideoId { get; set; }

        public int CommunityId { get; set; }

        public int VideoId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool? Active { get; set; }

        public virtual Community Community { get; set; }

        public virtual Video Video { get; set; }
    }
}
