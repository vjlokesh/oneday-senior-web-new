namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("UserProfile")]
    public partial class UserProfile
    {
        [Key]
        public int UserProfileId { get; set; }

        public int UserId { get; set; }

        [StringLength(500)]
        public string PhotoPath { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual User User { get; set; }
    }
}
