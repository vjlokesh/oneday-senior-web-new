namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class CategoryCustomization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CategoryCustomization()
        {
            StoryCustomizations = new HashSet<StoryCustomization>();
            Communities = new HashSet<Community>();
        }

        [Key]
        public int CategoryCustomizationId { get; set; }

        public int CmsCategoryId { get; set; }

        public int BoardId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public int? SortOrder { get; set; }

        public virtual Board Board { get; set; }

        public virtual CmsCategory CmsCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StoryCustomization> StoryCustomizations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Community> Communities { get; set; }
    }
}
