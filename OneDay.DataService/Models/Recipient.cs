namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Recipient")]
    public partial class Recipient
    {
        [Key]
        public int RecipientId { get; set; }

        public int RecipientUserId { get; set; }

        public int SenderUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? VideoId { get; set; }

        public bool IsActive { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual Video Video { get; set; }
    }
}
