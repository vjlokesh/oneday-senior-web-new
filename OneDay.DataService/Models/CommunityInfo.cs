namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CommunityInfo")]
    public partial class CommunityInfo
    {
        [Key]
        public int CommunityInfoId { get; set; }

        public int? CompanyId { get; set; }

        public int? CommunityId { get; set; }

        [StringLength(300)]
        public string Header { get; set; }

        public string InfoBody { get; set; }

        [StringLength(500)]
        public string Footer { get; set; }

        [StringLength(500)]
        public string FooterLink { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedByUserId { get; set; }

        public byte StatusId { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int? BlockNumber { get; set; }

        public virtual Community Community { get; set; }

        public virtual Status Status { get; set; }
    }
}
