﻿namespace OneDay.DataService.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DeviceTokens")]
    public partial class DeviceToken
    {
        [Key]
        public int DeviceTokenId { get; set; }

        public int UserId { get; set; }

        public string Token { get; set; }

        public string DdeviceName { get; set; }

        public string Platform { get; set; }

        public virtual User User { get; set; }
    }
}