namespace OneDay.DataService.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Token")]
    public partial class Token
    {
        [Key]
        public int TokenId { get; set; }

        public int UserId { get; set; }

        public Guid AuthToken { get; set; }

        public DateTime IssuedOn { get; set; }

        public DateTime ExpiresOn { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid? IappId { get; set; }

        public virtual User User { get; set; }
    }
}
