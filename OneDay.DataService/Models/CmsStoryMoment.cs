namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsStoryMoment")]
    public partial class CmsStoryMoment
    {
        [Key]
        public int CmsStoryMomentId { get; set; }

        public int CmsStoryId { get; set; }

        public int CmsMomentId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public short SortOrder { get; set; }

        public virtual CmsMoment CmsMoment { get; set; }

        public virtual CmsStory CmsStory { get; set; }
    }
}
