namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("CmsTheme")]
    public partial class CmsTheme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CmsTheme()
        {
            ThemeCustomizations = new HashSet<ThemeCustomization>();
        }

        [Key]
        public int CmsThemeId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        public int CompanyId { get; set; }

        [StringLength(255)]
        public string BackgroundColor { get; set; }

        [StringLength(255)]
        public string TitleColor { get; set; }

        [StringLength(255)]
        public string TitleFont { get; set; }

        public int? CmsAudioId { get; set; }

        public int? BackgroundImageId { get; set; }

        public byte? StatusId { get; set; }

        public int? IntroImageId { get; set; }

        public int? TitleBackgroundImageId { get; set; }

        public int CreatedByUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? PublishedByUserId { get; set; }

        public DateTime? PublishedDate { get; set; }

        public int? ArchivedByUserId { get; set; }

        public DateTime? ArchivedDate { get; set; }

        public int? OutroImageId { get; set; }

        public virtual CmsAudio CmsAudio { get; set; }

        public virtual CmsImage CmsImage { get; set; }

        public virtual CmsImage CmsImage1 { get; set; }

        public virtual CmsImage CmsImage2 { get; set; }

        public virtual CmsImage CmsImage3 { get; set; }

        public virtual Company Company { get; set; }

        public virtual Status Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThemeCustomization> ThemeCustomizations { get; set; }
    }
}
