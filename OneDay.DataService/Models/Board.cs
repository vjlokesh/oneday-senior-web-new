namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class Board
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Board()
        {
            CategoryCustomizations = new HashSet<CategoryCustomization>();
        }

        [Key]
        public int BoardId { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        public DateTime PublishedDate { get; set; }

        public int CompanyId { get; set; }

        public bool Active { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategoryCustomization> CategoryCustomizations { get; set; }
    }
}
