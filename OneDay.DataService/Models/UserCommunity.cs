namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("UserCommunity")]
    public partial class UserCommunity
    {
        [Key]
        public int UserCommunityId { get; set; }

        public int UserId { get; set; }

        public int CommunityId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool Active { get; set; }

        public virtual Community Community { get; set; }

        public virtual User User { get; set; }
    }
}
