﻿namespace OneDay.DataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("InviatationDetails")]
    public partial class InviatationDetails
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InviatationDetails()
        {
            Confirmations = new HashSet<InvitationConfirmation>();
        }

        [Key]
        public int Id { get; set; }

        public Guid PublicId { get; set; }

        public Video Video { get; set; }

        public int VideoId { get; set; }

        public int RecipientUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int Clicked { get; set; }

        public virtual User Recipient { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvitationConfirmation> Confirmations { get; set; }
    }
}
