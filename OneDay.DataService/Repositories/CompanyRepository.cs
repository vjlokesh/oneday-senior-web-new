﻿using Dapper;
using OneDay.DataService.Abstract;
using OneDay.DataService.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using OneDay.DataService.Models;

namespace OneDay.DataService.Repositories
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(string connectionString) : base(connectionString)
        {

        }
        public List<CompanyStat> GetAllCompanies()
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CompanyId", null, DbType.Int32);
            parameters.Add("@UserId", 1);
            parameters.Add("@SortBy", "CreatedDate");
            parameters.Add("@IsOnedayAdmin", true);

            var companies = SqlMapper.Query<CompanyStat>(DataConnection, "CompanyStatGet", parameters, null, false, null, CommandType.StoredProcedure);
            return companies.ToList();

        }

        public void Dispose()
        {
            DataConnection.Dispose();
        }
    }
}
