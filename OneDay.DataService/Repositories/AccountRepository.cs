﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Dapper;
using OneDay.DataService.Abstract;
using System.Linq;
using OneDay.DataService.Models;

namespace OneDay.DataService.Repositories
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        public AccountRepository(string connectionString) : base(connectionString)
        {

        }

        public bool ValidateToken(Guid authToken, int authTokenExpiry, string iAppId = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@AuthToken", authToken, DbType.Guid);
            parameters.Add("@AuthTokenExpiry", authTokenExpiry);

            parameters.Add("@IappId", iAppId, DbType.Guid);
            parameters.Add("@IsValid", null, DbType.Boolean, ParameterDirection.Output);

            var eResult = SqlMapper.Query<int>(DataConnection, "TokenValidate", parameters, null, false, null,
                CommandType.StoredProcedure).FirstOrDefault();
            bool result = parameters.Get<bool>("@IsValid");
            return result;
        }

        public User FindByCredentials(string email, string password)
        {
            User user = null;
            int userId = 0;
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Email", email);
            parameters.Add("@Password", password);
            parameters.Add("@UserId", null, DbType.Int32, ParameterDirection.Output);
            var result = SqlMapper.Query<int>(DataConnection, "UserAuthenticate", parameters, null, false,null,CommandType.StoredProcedure).FirstOrDefault();
            userId= parameters.Get<int>("@UserId");

            if (userId == null || userId == 0)
                return null;

            DynamicParameters userParameters = new DynamicParameters();
            userParameters.Add("@Email", email);
            userParameters.Add("@UserId", userId);
            userParameters.Add("@IsLogin", null);

            user = SqlMapper.Query<User>(DataConnection, "UserGet", userParameters, null, false, null, CommandType.StoredProcedure).FirstOrDefault();

            return user;
        }

        public void Dispose()
        {
            DataConnection.Dispose();
        }
    }
}
