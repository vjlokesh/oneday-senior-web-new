﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OneDay.Utils
{
    public class ConfigurationHelper
    {
        private static IConfigurationRoot _configuration { get; set; }
        public static IConfigurationRoot Configuration { get
            {
                if (_configuration == null)
                    SetConfiguration();
                return _configuration;
            } }
        private static void SetConfiguration()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            _configuration = builder.Build();

        }

        public static string GetConnectionString()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            return configuration.GetConnectionString("DefaultConnection");

        }


    }
}
